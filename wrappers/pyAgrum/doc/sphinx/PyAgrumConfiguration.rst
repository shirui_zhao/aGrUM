Configuration for pyAgrum
=========================

Configuration for pyAgrum is centralized in an object ``gum.config``, singleton of the class ``PyAgrumConfiguration``.


.. autoclass:: pyAgrum.PyAgrumConfiguration